#!/usr/bin/env bash
set -o nounset
#set -o xtrace

die() {
    echo "$@" >&2
    exit 1
}

proc_protos() {
    protoc -I=${VOLUME_PROTO} ${VOLUME_PROTO}/${1}/*.proto \
    --rust_out=${SRC_DIR} \
    --plugin=protoc-gen-grpc=${GRPC_RUST_PLUGIN} \
    --grpc_out=${SRC_DIR}
}

GRPC_RUST_PLUGIN=$(command -v grpc_rust_plugin)

[[ ! -d ${VOLUME_PROTO} ]] && die 'Error: Proto volume not found!'
[[ -z ${GRPC_RUST_PLUGIN} ]] && die 'Error: gRPC plugin not found!'

cd $(dirname "$0")
SRC_DIR='./src/grpc'

find ${SRC_DIR} ! -name 'mod.rs' -type f -exec rm -f {} +

proc_protos "utils"
proc_protos "dago-sequencer"