use std::collections::HashMap;

use benchmark::Bencher;
use comparator::{extract_unique_edges, extract_unique_nodes};
use psql_service::models::{Edge, Node, Page};
use psql_service::PsqlService;
use BoxResult;

static LIFETIME_CREATED: i16 = 1;
static LIFETIME_REMOVED: i16 = 2;

#[derive(Clone)]
pub struct Sequencer {
    psql: PsqlService,
    bencher: Bencher,
}

impl Sequencer {
    pub fn new() -> BoxResult<Self> {
        let psql = PsqlService::new()?;

        Ok(Sequencer {
            psql,
            bencher: Bencher::new(),
        })
    }

    pub fn sequence_page(&mut self, page_id: u64) -> BoxResult<()> {
        // Fetching current page
        let current_page = self.psql.get_page(page_id as i64)?;

        // Fetch predecessor page
        let predecessor_page = self
            .psql
            .get_page_by_graph_id_and_number(current_page.graph_id, current_page.number - 1);

        // Sequencer is done if no predecessor could be found (current page is first page)
        if predecessor_page.is_err() {
            info!(
                "No predecessor found of page with id = {} - sequence processing done.",
                page_id
            );
            return Ok(());
        }

        let predecessor_page = predecessor_page.unwrap();

        self.process_node_sequence(&current_page, &predecessor_page)?;
        self.process_edge_sequence(&current_page, &predecessor_page)?;

        info!("Sequencer finished");
        Ok(())
    }

    /// Fetches all leaf-nodes and updates the lifetime of all deleted and created ones.
    fn process_node_sequence(
        &mut self,
        current_page: &Page,
        predecessor_page: &Page,
    ) -> BoxResult<()> {
        let mut predecessor_nodes = self.psql.get_leaf_nodes_by_page_id(predecessor_page.id)?;
        let mut current_nodes = self.psql.get_leaf_nodes_by_page_id(current_page.id)?;

        self.bencher.start();
        extract_unique_nodes(&mut predecessor_nodes, &mut current_nodes);
        self.bencher.measure("Extract unique nodes");

        info!(
            "Found {} created nodes in current page",
            current_nodes.len()
        );
        info!(
            "Found {} deleted nodes in last page",
            predecessor_nodes.len()
        );

        self.bencher
            .log(&format!("Nodes created : {}", current_nodes.len(),));
        self.bencher
            .log(&format!("Nodes deleted : {}", predecessor_nodes.len()));

        self.bencher.start();
        self.update_node_sequence(&predecessor_nodes, LIFETIME_REMOVED, predecessor_page.id)?;
        self.update_node_sequence(&current_nodes, LIFETIME_CREATED, current_page.id)?;
        self.bencher.measure("Update node-sequence");

        Ok(())
    }

    /// Updates the attribute-lifetime of all specified nodes and their parents.
    fn update_node_sequence(
        &self,
        nodes: &HashMap<i64, Node>,
        lifetime: i16,
        page_id: u64,
    ) -> BoxResult<()> {
        let mut quad_tree_ids = Vec::new();

        for (_, node) in nodes.iter() {
            let mut relations = split_quad_tree_id(node.attribute.quad_tree_id);
            quad_tree_ids.append(&mut relations);
        }

        // Remove duplicates
        quad_tree_ids.sort();
        quad_tree_ids.dedup_by(|a, b| a == b);

        let quad_tree_ids = &vec_to_string(&quad_tree_ids);

        if quad_tree_ids.len() > 0 {
            self.psql
                .increase_attributes_lifetime_of_node_by_quad_tree_id(
                    page_id,
                    lifetime,
                    &quad_tree_ids,
                )?;
        }

        Ok(())
    }

    fn process_edge_sequence(
        &mut self,
        current_page: &Page,
        predecessor_page: &Page,
    ) -> BoxResult<()> {
        let mut predecessor_edges = self
            .psql
            .get_edges_by_page_and_depth(predecessor_page.id, predecessor_page.max_depth)?;
        let mut current_edges = self
            .psql
            .get_edges_by_page_and_depth(current_page.id, current_page.max_depth)?;

        self.bencher.start();
        extract_unique_edges(&mut predecessor_edges, &mut current_edges);
        self.bencher.measure("Extract unique edges");

        info!("Found {} newly created edges", current_edges.len());
        info!("Found {} newly deleted edges", predecessor_edges.len());

        self.bencher
            .log(&format!("Edges created : {}", current_edges.len()));
        self.bencher
            .log(&format!("Edges deleted : {}", predecessor_edges.len()));

        self.bencher.start();
        self.update_edge_sequence(&predecessor_edges, LIFETIME_REMOVED)?;
        self.update_edge_sequence(&current_edges, LIFETIME_CREATED)?;
        self.bencher.measure("Update edge-sequence");

        Ok(())
    }

    /// Updates the attribute-lifetime of all specified nodes and their parents.
    fn update_edge_sequence(&self, edges: &HashMap<u64, Edge>, lifetime: i16) -> BoxResult<()> {
        let mut page_id = None;
        let mut all_start_relations = Vec::new();
        let mut all_end_relations = Vec::new();

        for (_, edge) in edges.iter() {
            if page_id.is_none() {
                page_id = Some(edge.page_id);
            }

            let mut start_relations = split_quad_tree_id(edge.start_node.attribute.quad_tree_id);
            let mut end_relations = &mut split_quad_tree_id(edge.end_node.attribute.quad_tree_id);

            all_start_relations.append(&mut start_relations);
            all_end_relations.append(&mut end_relations);
        }

        // Remove duplicated ids from relations
        all_start_relations.sort();
        all_start_relations.dedup_by(|a, b| a == b);

        all_end_relations.sort();
        all_end_relations.dedup_by(|a, b| a == b);

        // Convert vector relations into comma separated string
        let start_quad_tree_ids = &vec_to_string(&all_start_relations);
        let end_quad_tree_ids = &vec_to_string(&all_end_relations);

        if let Some(page_id) = page_id {
            self.psql.update_attributes_lifetime_of_edge(
                page_id,
                lifetime,
                start_quad_tree_ids,
                end_quad_tree_ids,
            )?;
        }

        Ok(())
    }
}

/// Extracts the parental relations out of the quad_tree_id, including the id itself.
fn split_quad_tree_id(quad_tree_id: u64) -> Vec<u64> {
    let mut rel = Vec::new();
    for digit in quad_tree_id.to_string().chars() {
        let mut val = digit
            .to_digit(10)
            .expect(&format!("Invalid quad_tree_id detected: {}", quad_tree_id));

        if !rel.is_empty() {
            let val_str = &format!("{}{}", rel.get(rel.len() - 1).unwrap(), val);
            val = val_str.parse().unwrap();
        }

        rel.push(val as u64);
    }

    rel
}

/// Concatenates all numbers, separated by a comma
fn vec_to_string(vec: &Vec<u64>) -> String {
    let mut vec_str = String::from("");

    for number in vec.iter() {
        vec_str.push_str(&(*number).to_string());
        vec_str.push_str(",");
    }

    vec_str.pop(); // Remove last `,`

    vec_str
}

#[cfg(test)]
mod tests {
    #[test]
    fn split_quad_tree_id() {
        let qt_id = 432142113u64;
        let rel = super::split_quad_tree_id(qt_id);

        assert_eq!(rel.len(), 9);
        assert_eq!(rel.get(0), Some(&4u64));
        assert_eq!(rel.get(1), Some(&43u64));
        assert_eq!(rel.get(2), Some(&432u64));
        assert_eq!(rel.get(3), Some(&4321u64));
        assert_eq!(rel.get(4), Some(&43214u64));
        assert_eq!(rel.get(5), Some(&432142u64));
        assert_eq!(rel.get(6), Some(&4321421u64));
        assert_eq!(rel.get(7), Some(&43214211u64));
        assert_eq!(rel.get(8), Some(&432142113u64));
    }

    #[test]
    fn vec_to_string() {
        let vec: Vec<u64> = vec![
            4, 43, 432, 4321, 43214, 432142, 4321421, 43214211, 432142113,
        ];
        let str = super::vec_to_string(&vec);

        assert_eq!(str, "4,43,432,4321,43214,432142,4321421,43214211,432142113")
    }

    #[test]
    fn vec_to_string_of_empty_vector() {
        let vec: Vec<u64> = vec![];
        let str = super::vec_to_string(&vec);

        assert_eq!(str, "")
    }
}
