use std::collections::HashMap;
use std::env;

use postgres::{Connection, TlsMode};

use psql_service::mappers::{map_attribute, map_page};
use psql_service::models::{Attribute, Edge, Node, Page};
use BoxResult;

static ENV_PSQL_HOST: &str = "POSTGRES_HOST";
static ENV_PSQL_PORT: &str = "POSTGRES_PORT";
static ENV_PSQL_DB: &str = "POSTGRES_DB";
static ENV_PSQL_USER: &str = "POSTGRES_USER";
static ENV_PSQL_PWD: &str = "POSTGRES_PASSWORD";

pub struct PsqlService {
    conn: Connection,
}

impl PsqlService {
    pub fn new() -> BoxResult<Self> {
        let url = create_url()?;
        debug!("Connecting to Postgres via '{}'", url);

        let conn = Connection::connect(url.as_str(), TlsMode::None)
            .map_err(|e| format!("Could not connect to Postgres: {}", e))?;

        Ok(PsqlService { conn })
    }

    /// Returns the page with the given id
    pub fn get_page(&self, id: i64) -> BoxResult<Page> {
        debug!("Fetching page with id = {}", id);

        self.conn
            .query(
                "SELECT id, graph_id, number, max_depth \
                 FROM page \
                 WHERE id = $1",
                &[&id],
            )
            .map_err(|e| format!("Could not fetch page with id = {}: {}", id, e).into())
            .and_then(|rows| {
                map_page(rows).ok_or(format!("Could not map page with id = {}", id).into())
            })
    }

    /// Returns the page of the specified graph of the given number
    pub fn get_page_by_graph_id_and_number(&self, graph_id: u64, number: u16) -> BoxResult<Page> {
        debug!(
            "Fetching page with number = {} of graph with id = {}",
            number, graph_id
        );

        self.conn
            .query(
                "SELECT id, graph_id, number, max_depth \
                 FROM page \
                 WHERE graph_id = $1 AND number = $2",
                &[&(graph_id as i64), &(number as i16)],
            )
            .map_err(|e| {
                format!(
                    "Could not fetch page of graph = {} and number = {}: {}",
                    graph_id, number, e
                )
                .into()
            })
            .and_then(|rows| {
                map_page(rows).ok_or(
                    format!(
                        "Could not map page of graph = {} and number = {}",
                        graph_id, number
                    )
                    .into(),
                )
            })
    }

    /// Returns all leaf-nodes and their attributes of the given page with a set attribute-data_id.
    /// The keys of the map are the data-ids of the respective node-attribute.
    pub fn get_leaf_nodes_by_page_id(&self, page_id: u64) -> BoxResult<HashMap<i64, Node>> {
        debug!("Fetching all leaf nodes of page with id = {}", page_id);

        self.conn
            .query(
                "SELECT id, attribute_id \
                 FROM node \
                 WHERE page_id = $1 AND leaf",
                &[&(page_id as i64)],
            )
            .map_err(|e| {
                format!("Could not fetch nodes of page with id = {}: {}", page_id, e).into()
            })
            .and_then(|rows| {
                let mut nodes = HashMap::new();

                for row in rows.iter() {
                    if row.len() == 2 {
                        let attribute_id: i64 = row.get(1);
                        let attribute = self.get_attribute(attribute_id as u64)?;

                        let node_id: i64 = row.get(0);
                        let node = Node {
                            id: node_id as u64,
                            attribute,
                        };

                        if let Some(data_id) = node.attribute.data_id {
                            nodes.insert(data_id, node);
                        } else {
                            debug!(
                                "Could not find any data-id for node with id = {} - skipping node!",
                                node.id
                            );
                        }
                    } else {
                        error!("Could not map node");
                    }
                }

                debug!("Fetched {} nodes successfully", nodes.len());
                Ok(nodes)
            })
    }

    /// Returns the attribute with the given id.
    pub fn get_attribute(&self, id: u64) -> BoxResult<Attribute> {
        self.conn
            .query(
                "SELECT id, data_id, quad_tree, lifetime \
                 FROM attribute \
                 WHERE id = $1",
                &[&(id as i64)],
            )
            .map_err(|e| format!("Could not fetch attribute with id = {}: {}", id, e).into())
            .and_then(|rows| {
                map_attribute(rows)
                    .ok_or(format!("Could not map attribute with id = {}", id).into())
            })
    }

    /// Returns the attribute of the node with the given id.
    pub fn get_attribute_by_node_id(&self, node_id: u64) -> BoxResult<Attribute> {
        self.conn
            .query(
                "SELECT a.id, a.data_id, a.quad_tree, a.lifetime \
                 FROM node n INNER JOIN attribute a \
                 ON n.attribute_id = a.id \
                 WHERE n.id = $1",
                &[&(node_id as i64)],
            )
            .map_err(|e| {
                format!(
                    "Could not fetch attribute of node with id = {}: {}",
                    node_id, e
                )
                .into()
            })
            .and_then(|rows| {
                map_attribute(rows)
                    .ok_or(format!("Could not map attribute of node with id = {}", node_id).into())
            })
    }

    /// Sets the lifetime value of all node-attributes to the given value, selected by the given quad_tree_id.
    pub fn increase_attributes_lifetime_of_node_by_quad_tree_id(
        &self,
        page_id: u64,
        lifetime: i16,
        ids: &String,
    ) -> BoxResult<()> {
        debug!(
            "Set lifetime to '{}' of node-attributes with quad_tree-ids {}",
            lifetime, ids
        );

        self.conn
            .query(
                &format!(
                    "UPDATE attribute \
                     SET lifetime = lifetime + $1 \
                     WHERE id IN ( \
                     SELECT a.id \
                     FROM node n INNER JOIN attribute a \
                     ON n.attribute_id = a.id \
                     WHERE page_id = $2 AND quad_tree IN ({}) \
                     )",
                    ids
                ), // Not pretty but it works
                &[&lifetime, &(page_id as i64)],
            )
            .map_err(|e| {
                format!(
                    "Could not update attribute-lifetime of quad_tree_ids = {}: {}",
                    ids, e
                )
                .into()
            })
            .and_then(|_| Ok(()))
    }

    /// Sets the lifetime value of all edge-attributes to the given value, when the start- or
    /// end-node has either the quad_tree-id specified in ids_1 or ids_2
    ///
    /// Note:
    /// This is necessary to find all edges in both directions, from start to end AND from end
    /// to start!
    pub fn update_attributes_lifetime_of_edge(
        &self,
        page_id: u64,
        lifetime: i16,
        ids_1: &String,
        ids_2: &String,
    ) -> BoxResult<()> {
        debug!(
            "Set lifetime to '{}' of edge-attributes of points with quad_tree-ids {} and {}",
            lifetime, ids_1, ids_2
        );

        self.conn
            .query(
                &format!(
                    "WITH start_nodes AS ( \
                     SELECT n.id \
                     FROM node n INNER JOIN attribute a \
                     ON n.attribute_id = a.id \
                     WHERE n.page_id = $1 AND a.quad_tree in ({}) \
                     ), end_nodes AS ( \
                     SELECT n.id \
                     FROM node n INNER JOIN attribute a \
                     ON n.attribute_id = a.id \
                     WHERE n.page_id = $1 AND a.quad_tree in ({}) \
                     ) \
                     \
                     UPDATE attribute \
                     SET lifetime = lifetime + $2 \
                     WHERE id IN ( \
                     SELECT a.id \
                     FROM edge e INNER JOIN attribute a \
                     ON e.attribute_id = a.id \
                     WHERE ( \
                     start_node_id IN ( SELECT * FROM start_nodes ) \
                     AND end_node_id IN ( SELECT * FROM end_nodes) \
                     ) OR ( \
                     start_node_id IN ( SELECT * FROM end_nodes ) \
                     AND end_node_id IN ( SELECT * FROM start_nodes) \
                     ) \
                     )",
                    ids_1, ids_2
                ), // Not pretty but it works
                &[&(page_id as i64), &lifetime],
            )
            .map_err(|e| {
                format!(
                    "Could not update attribute-lifetime of edges with points = {} / {}: {}",
                    ids_1, ids_2, e
                )
                .into()
            })
            .and_then(|_| Ok(()))
    }

    /// Returns all edges of the given page and depth with edge-id as key.
    pub fn get_edges_by_page_and_depth(
        &self,
        page_id: u64,
        depth: u16,
    ) -> BoxResult<HashMap<u64, Edge>> {
        debug!(
            "Fetch all edges of page with id = {} and depth = {}",
            page_id, depth
        );

        self.conn
            .query(
                "SELECT id, page_id, start_node_id, end_node_id \
                 FROM edge \
                 WHERE page_id = $1 AND depth = $2",
                &[&(page_id as i64), &(depth as i16)],
            )
            .map_err(|e| {
                format!(
                    "Could not fetch edges of page with id = {} and depth = {}: {}",
                    page_id, depth, e
                )
                .into()
            })
            .and_then(|rows| {
                let mut edges = HashMap::new();

                for row in rows.iter() {
                    if row.len() == 4 {
                        let start_node_id: i64 = row.get(2);
                        let end_node_id: i64 = row.get(3);

                        let start_attribute =
                            self.get_attribute_by_node_id(start_node_id as u64)?;
                        let end_attribute = self.get_attribute_by_node_id(end_node_id as u64)?;

                        let start_node = Node {
                            id: start_node_id as u64,
                            attribute: start_attribute,
                        };
                        let end_node = Node {
                            id: end_node_id as u64,
                            attribute: end_attribute,
                        };

                        if start_node.attribute.data_id.as_ref().is_some()
                            && end_node.attribute.data_id.as_ref().is_some()
                        {
                            let id: i64 = row.get(0);
                            let page_id: i64 = row.get(1);

                            let edge = Edge {
                                id: id as u64,
                                page_id: page_id as u64,
                                start_node,
                                end_node,
                            };

                            edges.insert(edge.id, edge);
                        } else {
                            debug!(
                                "Could not find any data-id for node {} and/or node {} \
                                 - skipping node!",
                                start_node.id, end_node.id
                            );
                        }
                    } else {
                        error!("Could not map edge");
                    }
                }

                debug!("Fetched {} edges successfully", edges.len());
                Ok(edges)
            })
    }
}

impl Clone for PsqlService {
    fn clone(&self) -> Self {
        PsqlService::new().expect("Error cloning psql service")
    }
}

/// Creates a URL to the redis service in the format:
/// "redis://<user>:<password>@<endpoint>:<port>/<database>"
fn create_url() -> BoxResult<String> {
    if let (Ok(host), Ok(port), Ok(db), Ok(user), Ok(pwd)) = (
        env::var(ENV_PSQL_HOST),
        env::var(ENV_PSQL_PORT),
        env::var(ENV_PSQL_DB),
        env::var(ENV_PSQL_USER),
        env::var(ENV_PSQL_PWD),
    ) {
        Ok(format!(
            "postgres://{}:{}@{}:{}/{}",
            user, pwd, host, port, db
        ))
    } else {
        Err("Could not parse environment variable for postgres connection".into())
    }
}

/// Contains all needed mappers to convert database query results into the respective models.
/// Requires row to contain all data defined in the respective model.
mod mappers {
    use postgres::rows::Rows;

    use psql_service::models::{Attribute, Page};

    /// Maps into a page model.
    pub fn map_page(rows: Rows) -> Option<Page> {
        if rows.len() >= 1 {
            let row = rows.get(0);
            if row.len() == 4 {
                let id: i64 = row.get(0);
                let graph_id: i64 = row.get(1);
                let number: i16 = row.get(2);
                let max_depth: i16 = row.get(3);

                return Some(Page {
                    id: id as u64,
                    graph_id: graph_id as u64,
                    number: number as u16,
                    max_depth: max_depth as u16,
                });
            }
        }

        None
    }

    /// Maps into attribute model-
    pub fn map_attribute(rows: Rows) -> Option<Attribute> {
        if rows.len() >= 1 {
            let row = rows.get(0);
            if row.len() == 4 {
                let id: i64 = row.get(0);
                let quad_tree_id: i64 = row.get(2);
                let lifetime: i16 = row.get(3);

                return Some(Attribute {
                    id: id as u64,
                    data_id: row.get(1),
                    quad_tree_id: quad_tree_id as u64,
                    lifetime: lifetime as u16,
                });
            }
        }

        None
    }
}

/// Contains all needed models of the DAGO-database.
pub mod models {
    #[derive(Debug)]
    pub struct Page {
        pub id: u64,
        pub graph_id: u64,
        pub number: u16,
        pub max_depth: u16,
    }

    #[derive(Debug)]
    pub struct Node {
        pub id: u64,
        pub attribute: Attribute,
    }

    #[derive(Debug)]
    pub struct Edge {
        pub id: u64,
        pub page_id: u64,
        pub start_node: Node,
        pub end_node: Node,
    }

    #[derive(Debug)]
    pub struct Attribute {
        pub id: u64,
        pub data_id: Option<i64>,
        pub quad_tree_id: u64,
        pub lifetime: u16,
    }
}
