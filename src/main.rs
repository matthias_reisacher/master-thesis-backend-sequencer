extern crate core;
extern crate env_logger;
extern crate futures;
extern crate grpcio;
extern crate grpcio_proto;
#[macro_use]
extern crate log;
extern crate postgres;
extern crate proc_macro;
extern crate protobuf;

use std::env;
use std::net::TcpListener;
use std::sync::Arc;

use env_logger::Builder;
use grpcio::{Environment, ServerBuilder};

use api::SequencePageServiceImpl;
use grpc::sequence_page_grpc::create_sequence_page_service;
use page_sequencer::Sequencer;
use psql_service::PsqlService;

mod api;
mod benchmark;
mod comparator;
mod grpc;
mod page_sequencer;
mod psql_service;

type BoxResult<T> = Result<T, Box<dyn std::error::Error>>;

static ENV_LOG_LEVEL: &str = "LOG_LEVEL";
static ENV_PORT: &str = "GRPC_PORT";

static DEFAULT_HOST: &str = "0.0.0.0";

struct Server<'a> {
    host: &'a str,
    port: u16,
}

impl<'a> Server<'a> {
    pub fn run(&self) {
        let env = Arc::new(Environment::new(1));

        let service_impl = SequencePageServiceImpl::new()
            .map_err(|e| panic!("Could not instantiate gRPC Service: {}", e))
            .unwrap();
        let service = create_sequence_page_service(service_impl);

        let mut server = ServerBuilder::new(env)
            .register_service(service)
            .bind(self.host, self.port)
            .build()
            .unwrap();

        server.start();
        for &(ref host, port) in server.bind_addrs() {
            info!("Listening on {}:{}", host, port);
        }

        // Keep server up indefinitely
        loop {}
    }
}

fn main() -> BoxResult<()> {
    Builder::new()
        .parse_filters(&env::var(ENV_LOG_LEVEL).unwrap())
        .init();

    let port = match env::var(ENV_PORT) {
        Ok(arg) => arg.parse::<u16>().unwrap(),
        Err(_) => get_available_port().unwrap(),
    };

    let server = Server {
        host: DEFAULT_HOST,
        port,
    };

    server.run();
    Ok(())
}

fn get_available_port() -> Option<u16> {
    (8000..9999).find(|port| port_is_available(*port))
}

fn port_is_available(port: u16) -> bool {
    match TcpListener::bind((DEFAULT_HOST, port)) {
        Ok(_) => true,
        Err(_) => false,
    }
}
