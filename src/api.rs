use futures::Future;
use grpcio::{RpcContext, UnarySink};

use benchmark::Bencher;
use grpc::sequence_page::{SequencePageRequest, SequencePageResponse};
use grpc::sequence_page_grpc::SequencePageService;
use page_sequencer::Sequencer;
use BoxResult;

#[derive(Clone)]
pub struct SequencePageServiceImpl {
    sequencer: Sequencer,
    bencher: Bencher,
}

impl SequencePageServiceImpl {
    /// Create new SequencePageServiceImpl
    pub fn new() -> BoxResult<Self> {
        let sequencer = Sequencer::new()?;

        Ok(SequencePageServiceImpl {
            sequencer,
            bencher: Bencher::new(),
        })
    }
}

impl SequencePageService for SequencePageServiceImpl {
    fn process(
        &mut self,
        ctx: RpcContext,
        req: SequencePageRequest,
        sink: UnarySink<SequencePageResponse>,
    ) {
        info!("Received sequence-request: {:?}", req);
        self.bencher.start();

        let result = self
            .sequencer
            .sequence_page(req.page_id)
            .map_err(|e| error!("Could not process received request: {}", e));
        info!("Sequencing of page with id={} completed!", req.page_id);

        let mut response = SequencePageResponse::new();
        response.set_result(result.is_ok());

        self.bencher.measure("Total processing time");

        let f = sink
            .success(response)
            .map_err(move |e| error!("Failed to reply: {:?}: {:?}", req, e));
        ctx.spawn(f);
    }
}
