use std::cmp::Ordering;
use std::collections::HashMap;

use psql_service::models::{Edge, Node};

/// Compares the keys of both maps and removes all non-unique entries from both lists.
pub fn extract_unique_nodes(a: &mut HashMap<i64, Node>, b: &mut HashMap<i64, Node>) {
    let diffs = compare_node_ids(
        &mut a.keys().collect::<Vec<&i64>>(),
        &mut b.keys().collect::<Vec<&i64>>(),
    );

    // Remove all non-unique elements from the maps
    a.retain(|&id, _| diffs.0.contains(&id));
    b.retain(|&id, _| diffs.1.contains(&id));
}

/// Sorts the node ids, compares them and returns their differences.
fn compare_node_ids(a: &mut Vec<&i64>, b: &mut Vec<&i64>) -> (Vec<i64>, Vec<i64>) {
    a.sort();
    b.sort();

    let mut a_diff = Vec::new();
    let mut b_diff = Vec::new();

    while !a.is_empty() || !b.is_empty() {
        compare_last_number_of_sorted_list(a, b, &mut a_diff, &mut b_diff);
    }

    (a_diff, b_diff)
}

/// Compares the last elements of list a and b.
/// If the elements are not equal, the value is added to the respective diff list.
/// Compared elements are removed form the lists a and b.
fn compare_last_number_of_sorted_list(
    a: &mut Vec<&i64>,
    b: &mut Vec<&i64>,
    a_diff: &mut Vec<i64>,
    b_diff: &mut Vec<i64>,
) {
    if a.len() > 0 && b.len() > 0 {
        let last_a = a.get(a.len() - 1).unwrap();
        let last_b = b.get(b.len() - 1).unwrap();

        // Since we are working with a sorted list and comparing from back to front, we have to
        // eliminate the largest value.
        if last_a == last_b {
            a.pop();
            b.pop();
        } else if last_a > last_b {
            a_diff.push(*(a.pop().unwrap()));
        } else {
            b_diff.push(*(b.pop().unwrap()));
        }
    } else if a.len() > 0 && b.len() == 0 {
        a_diff.push(*(a.pop().unwrap()));
    } else if a.len() == 0 && b.len() > 0 {
        b_diff.push(*(b.pop().unwrap()));
    }
}

/// Compares both maps and removes all non-unique entries from both lists.
pub fn extract_unique_edges(a: &mut HashMap<u64, Edge>, b: &mut HashMap<u64, Edge>) {
    // Simplify edges for faster comparing algorithm
    let a_edge_points = build_edge_points(a);
    let b_edge_points = build_edge_points(b);

    let diffs = compare_simplified_edges(
        &mut edge_map_to_tuple_list(&a_edge_points),
        &mut edge_map_to_tuple_list(&b_edge_points),
    );

    a.retain(|&id, _| diffs.0.contains(&id));
    b.retain(|&id, _| diffs.1.contains(&id));
}

/// Converts each edge into a simplified format usable for the comparison algorithm.
/// Each edge is represented by the data_id of it's start- and end-points, whereby the smaller
/// data_id is used as starting point and the larger data_id as the ending point.
fn build_edge_points(edges: &HashMap<u64, Edge>) -> HashMap<u64, String> {
    let mut edge_points = HashMap::new();
    for (id, edge) in edges {
        let start_data_id = edge.start_node.attribute.data_id.unwrap();
        let end_data_id = edge.end_node.attribute.data_id.unwrap();

        // Order start-/end-points to simplify the compare algorithm
        let edge_point = format!(
            "{}-{}",
            i64::min(start_data_id, end_data_id),
            i64::max(start_data_id, end_data_id)
        );

        edge_points.insert(*id, edge_point);
    }

    edge_points
}

/// Creates a list of the tuple (key, value) out of the given HashMap.
fn edge_map_to_tuple_list(edges: &HashMap<u64, String>) -> Vec<(&u64, &String)> {
    let mut tuple_list = Vec::new();
    for (id, edge) in edges {
        tuple_list.push((id, edge));
    }

    tuple_list
}

/// Sorts the node ids, compares them and returns the number (first part of tuple) of
/// their differences.
fn compare_simplified_edges(
    a: &mut Vec<(&u64, &String)>,
    b: &mut Vec<(&u64, &String)>,
) -> (Vec<u64>, Vec<u64>) {
    sort_simplified_edges(a, b);

    let mut a_diff = Vec::new();
    let mut b_diff = Vec::new();

    while !a.is_empty() || !b.is_empty() {
        compare_last_string_of_sorted_list(a, b, &mut a_diff, &mut b_diff);
    }

    (a_diff, b_diff)
}

/// Sorts the simplified edges by their starting point.
/// If the starting points are similar, the endpoints are compared.
/// The order is always ascending.
fn sort_simplified_edges(a: &mut Vec<(&u64, &String)>, b: &mut Vec<(&u64, &String)>) {
    let sort_simplified_edges = |x: &(&u64, &String), y: &(&u64, &String)| {
        let x_split: Vec<&str> = x.1.split("-").collect();
        let y_split: Vec<&str> = y.1.split("-").collect();

        let x_start = x_split.get(0).unwrap().parse::<i64>().unwrap();
        let y_start = y_split.get(0).unwrap().parse::<i64>().unwrap();

        if x_start < y_start {
            Ordering::Less
        } else if x_start > y_start {
            Ordering::Greater
        } else {
            let x_end = x_split.get(1).unwrap().parse::<i64>().unwrap();
            let y_end = y_split.get(1).unwrap().parse::<i64>().unwrap();

            if x_end < y_end {
                Ordering::Less
            } else if x_end > y_end {
                Ordering::Greater
            } else {
                Ordering::Equal
            }
        }
    };

    a.sort_by(sort_simplified_edges);
    b.sort_by(sort_simplified_edges);
}

/// Compares the last text-elements of list a and b.
/// If the elements are not equal, the number (first part of tuple) is added to
/// the respective diff list.
/// Compared elements are removed form the lists a and b.
fn compare_last_string_of_sorted_list(
    a: &mut Vec<(&u64, &String)>,
    b: &mut Vec<(&u64, &String)>,
    a_diff: &mut Vec<u64>,
    b_diff: &mut Vec<u64>,
) {
    if a.len() > 0 && b.len() > 0 {
        let last_a = *(a.get(a.len() - 1).unwrap());
        let last_b = *(b.get(b.len() - 1).unwrap());

        // Since we are working with a sorted list and comparing from back to front, we have to
        // eliminate the largest value.
        if last_a.1.eq(last_b.1) {
            a.pop();
            b.pop();
        } else if last_a.1.gt(last_b.1) {
            a_diff.push(*(a.pop().unwrap().0));
        } else {
            b_diff.push(*(b.pop().unwrap().0));
        }
    } else if a.len() > 0 && b.len() == 0 {
        a_diff.push(*(a.pop().unwrap().0));
    } else if a.len() == 0 && b.len() > 0 {
        b_diff.push(*(b.pop().unwrap().0));
    }
}

#[cfg(test)]
mod tests {
    use psql_service::models::Attribute;

    use super::*;

    fn create_node(id: u64, data_id: i64) -> Node {
        Node {
            id,
            attribute: Attribute {
                id,
                data_id: Some(data_id),
                quad_tree_id: id as u64,
                lifetime: 0,
            },
        }
    }

    fn create_edge(id: u64, start: Node, end: Node) -> Edge {
        Edge {
            id,
            page_id: 1,
            start_node: start,
            end_node: end,
        }
    }

    #[test]
    fn extract_unique_nodes() {
        let mut a = HashMap::new();
        a.insert(1i64, create_node(231u64, 1i64));
        a.insert(2i64, create_node(232u64, 2i64));
        a.insert(3i64, create_node(233u64, 3i64));

        let mut b = HashMap::new();
        b.insert(5i64, create_node(237u64, 5i64));
        b.insert(2i64, create_node(234u64, 2i64));
        b.insert(3i64, create_node(235u64, 3i64));
        b.insert(4i64, create_node(236u64, 4i64));

        super::extract_unique_nodes(&mut a, &mut b);

        assert_eq!(a.len(), 1);
        assert_eq!(a.get(&1).unwrap().attribute.data_id, Some(1));
        assert_eq!(b.len(), 2);
        assert_eq!(b.get(&4).unwrap().attribute.data_id, Some(4));
        assert_eq!(b.get(&5).unwrap().attribute.data_id, Some(5));
    }

    #[test]
    fn compare_last_elements_of_sorted_list_of_empty_lists() {
        let mut a = Vec::new();
        let mut b = Vec::new();
        let mut a_diff = Vec::new();
        let mut b_diff = Vec::new();

        compare_last_number_of_sorted_list(&mut a, &mut b, &mut a_diff, &mut b_diff);

        assert_eq!(a.len(), 0);
        assert_eq!(b.len(), 0);
        assert_eq!(a_diff.len(), 0);
        assert_eq!(b_diff.len(), 0);
    }

    #[test]
    fn compare_last_elements_of_sorted_list_with_similar_elements() {
        let mut a = vec![&1i64, &2i64, &3i64];
        let mut b = vec![&1i64, &2i64, &3i64];
        let mut a_diff = Vec::new();
        let mut b_diff = Vec::new();

        compare_last_number_of_sorted_list(&mut a, &mut b, &mut a_diff, &mut b_diff);
        compare_last_number_of_sorted_list(&mut a, &mut b, &mut a_diff, &mut b_diff);
        compare_last_number_of_sorted_list(&mut a, &mut b, &mut a_diff, &mut b_diff);

        assert_eq!(a.len(), 0);
        assert_eq!(b.len(), 0);
        assert_eq!(a_diff.len(), 0);
        assert_eq!(b_diff.len(), 0);
    }

    #[test]
    fn compare_last_elements_of_sorted_list_with_different_elements() {
        let mut a = vec![&1i64, &2i64, &3i64];
        let mut b = vec![&2i64, &3i64, &4i64, &5i64];
        let mut a_diff = Vec::new();
        let mut b_diff = Vec::new();

        compare_last_number_of_sorted_list(&mut a, &mut b, &mut a_diff, &mut b_diff);
        compare_last_number_of_sorted_list(&mut a, &mut b, &mut a_diff, &mut b_diff);
        compare_last_number_of_sorted_list(&mut a, &mut b, &mut a_diff, &mut b_diff);
        compare_last_number_of_sorted_list(&mut a, &mut b, &mut a_diff, &mut b_diff);
        compare_last_number_of_sorted_list(&mut a, &mut b, &mut a_diff, &mut b_diff);

        assert_eq!(a.len(), 0);
        assert_eq!(b.len(), 0);
        assert_eq!(a_diff.len(), 1);
        assert_eq!(a_diff.get(0), Some(&1));
        assert_eq!(b_diff.len(), 2);
        assert_eq!(b_diff.get(0), Some(&5));
        assert_eq!(b_diff.get(1), Some(&4));
    }

    #[test]
    fn extract_unique_edges() {
        let mut a = HashMap::new();

        let edge = create_edge(42u64, create_node(231u64, 1i64), create_node(232u64, 2i64));
        a.insert(edge.id, edge);

        let edge = create_edge(43u64, create_node(232u64, 2i64), create_node(233u64, 3i64));
        a.insert(edge.id, edge);

        let edge = create_edge(44u64, create_node(233u64, 3i64), create_node(231u64, 1i64));
        a.insert(edge.id, edge);

        let mut b = HashMap::new();

        let edge = create_edge(45u64, create_node(302u64, 2i64), create_node(303u64, 3i64));
        b.insert(edge.id, edge);

        let edge = create_edge(46u64, create_node(303u64, 3i64), create_node(304u64, 4i64));
        b.insert(edge.id, edge);

        let edge = create_edge(47u64, create_node(303u64, 3i64), create_node(301u64, 1i64));
        b.insert(edge.id, edge);

        let edge = create_edge(48u64, create_node(304u64, 4i64), create_node(30u64, 2i64));
        b.insert(edge.id, edge);

        super::extract_unique_edges(&mut a, &mut b);

        assert_eq!(a.len(), 1);
        assert_eq!(a.get(&42).unwrap().start_node.attribute.data_id, Some(1));
        assert_eq!(a.get(&42).unwrap().end_node.attribute.data_id, Some(2));
        assert_eq!(b.len(), 2);
        assert_eq!(b.get(&46).unwrap().start_node.attribute.data_id, Some(3));
        assert_eq!(b.get(&46).unwrap().end_node.attribute.data_id, Some(4));
        assert_eq!(b.get(&48).unwrap().start_node.attribute.data_id, Some(4));
        assert_eq!(b.get(&48).unwrap().end_node.attribute.data_id, Some(2));
    }

    #[test]
    fn build_edge_points() {
        let mut edges = HashMap::new();

        let edge = create_edge(42u64, create_node(231u64, 1i64), create_node(232u64, 2i64));
        edges.insert(edge.id, edge);

        let edge = create_edge(43u64, create_node(233u64, 3i64), create_node(232u64, 2i64));
        edges.insert(edge.id, edge);

        let edge_points = super::build_edge_points(&edges);

        assert_eq!(edge_points.len(), 2);
        assert_eq!(edge_points.get(&42), Some(&String::from("1-2")));
        assert_eq!(edge_points.get(&43), Some(&String::from("2-3")));
    }

    #[test]
    fn sort_simplified_edges() {
        let a_s1 = String::from("41-67");
        let a_s2 = String::from("11-70");

        let b_s3 = String::from("1-15");
        let b_s4 = String::from("69-111");
        let b_s5 = String::from("69-101");

        let mut a = vec![(&1u64, &a_s1), (&2u64, &a_s2)];
        let mut b = vec![(&3u64, &b_s3), (&4u64, &b_s4), (&5u64, &b_s5)];

        super::sort_simplified_edges(&mut a, &mut b);

        assert_eq!(a.len(), 2);
        assert_eq!(a.get(0), Some(&(&2u64, &String::from("11-70"))));
        assert_eq!(a.get(1), Some(&(&1u64, &String::from("41-67"))));

        assert_eq!(b.len(), 3);
        assert_eq!(b.get(0), Some(&(&3u64, &String::from("1-15"))));
        assert_eq!(b.get(1), Some(&(&5u64, &String::from("69-101"))));
        assert_eq!(b.get(2), Some(&(&4u64, &String::from("69-111"))));
    }

    #[test]
    fn compare_last_string_of_sorted_list_of_empty_list() {
        let mut a = Vec::new();
        let mut b = Vec::new();
        let mut a_diff = Vec::new();
        let mut b_diff = Vec::new();

        compare_last_string_of_sorted_list(&mut a, &mut b, &mut a_diff, &mut b_diff);

        assert_eq!(a.len(), 0);
        assert_eq!(b.len(), 0);
        assert_eq!(a_diff.len(), 0);
        assert_eq!(b_diff.len(), 0);
    }

    #[test]
    fn compare_last_string_of_sorted_list_with_similar_elements() {
        let a_s1 = String::from("1-2");
        let a_s2 = String::from("2-3");
        let a_s3 = String::from("3-4");

        let b_s1 = String::from("1-2");
        let b_s2 = String::from("2-3");
        let b_s3 = String::from("3-4");

        let mut a = vec![(&1u64, &a_s1), (&2u64, &a_s2), (&3u64, &a_s3)];
        let mut b = vec![(&4u64, &b_s1), (&5u64, &b_s2), (&6u64, &b_s3)];
        let mut a_diff = Vec::new();
        let mut b_diff = Vec::new();

        compare_last_string_of_sorted_list(&mut a, &mut b, &mut a_diff, &mut b_diff);
        compare_last_string_of_sorted_list(&mut a, &mut b, &mut a_diff, &mut b_diff);
        compare_last_string_of_sorted_list(&mut a, &mut b, &mut a_diff, &mut b_diff);

        assert_eq!(a.len(), 0);
        assert_eq!(b.len(), 0);
        assert_eq!(a_diff.len(), 0);
        assert_eq!(b_diff.len(), 0);
    }

    #[test]
    fn compare_last_string_of_sorted_list_with_different_elements() {
        let a_s1 = String::from("1-2");
        let a_s2 = String::from("2-3");
        let a_s3 = String::from("3-4");

        let b_s1 = String::from("2-3");
        let b_s2 = String::from("3-4");
        let b_s3 = String::from("3-5");
        let b_s4 = String::from("5-2");

        let mut a = vec![(&1u64, &a_s1), (&2u64, &a_s2), (&3u64, &a_s3)];
        let mut b = vec![
            (&4u64, &b_s1),
            (&5u64, &b_s2),
            (&6u64, &b_s3),
            (&7u64, &b_s4),
        ];
        let mut a_diff = Vec::new();
        let mut b_diff = Vec::new();

        compare_last_string_of_sorted_list(&mut a, &mut b, &mut a_diff, &mut b_diff);
        compare_last_string_of_sorted_list(&mut a, &mut b, &mut a_diff, &mut b_diff);
        compare_last_string_of_sorted_list(&mut a, &mut b, &mut a_diff, &mut b_diff);
        compare_last_string_of_sorted_list(&mut a, &mut b, &mut a_diff, &mut b_diff);
        compare_last_string_of_sorted_list(&mut a, &mut b, &mut a_diff, &mut b_diff);

        assert_eq!(a.len(), 0);
        assert_eq!(b.len(), 0);
        assert_eq!(a_diff.len(), 1);
        assert_eq!(a_diff.get(0), Some(&1u64));
        assert_eq!(b_diff.len(), 2);
        assert_eq!(b_diff.get(0), Some(&7u64));
        assert_eq!(b_diff.get(1), Some(&6u64));
    }
}
