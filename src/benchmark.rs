use std::env;
use std::time::{Duration, Instant};

use env_logger::Builder;

use BoxResult;

static ENV_BENCH: &str = "BENCHMARK";

#[derive(Clone)]
pub struct Bencher {
    enabled: bool,
    counter: Option<Instant>,
}

impl Bencher {
    pub fn new() -> Self {
        let enabled = match env::var(ENV_BENCH) {
            Ok(arg) => arg.parse::<i32>().unwrap_or(0) > 0,
            Err(_) => false,
        };

        Bencher {
            enabled,
            counter: None,
        }
    }

    pub fn start(&mut self) {
        self.counter = Some(Instant::now());
    }

    pub fn measure(&mut self, msg: &str) {
        if self.enabled && self.counter.is_some() {
            let duration = Instant::now().duration_since(self.counter.unwrap());
            self.log(&format!("{} : {:?}", msg, duration));
            self.start();
        }
    }

    pub fn log(&self, msg: &str) {
        if self.enabled {
            error!("[BENCH] {}", msg);
        }
    }
}
