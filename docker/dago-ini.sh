#!/usr/bin/env bash
cd /home/dago
if [[ -f compile-protos.sh ]]; then
    ./compile-protos.sh
else
    (>&2 echo "Could not compile protos")
    exit 1
fi

[[ -f Cargo.toml ]] && cargo run