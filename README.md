# Setup
The gRPC server is based on the [pincap](https://github.com/pingcap/grpc-rs) project.
Therefore, the prerequisites described on their Github-wiki must be met to start this project.  
